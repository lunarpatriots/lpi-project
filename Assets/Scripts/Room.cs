﻿using UnityEngine;
using System.Collections;

namespace game {
	public class Room : MonoBehaviour {
		protected float noMovThreshold = 0.0001f;
		protected float noMovFrames = 3;
		protected float procTime = 5.0f, cleanTime = 3.0f, startTime;
		protected Vector3 slot;
		protected ServiceRoom serviceRoom;
		internal Character currentOccupier;
		internal bool isInRoom = false, occupied = false,
			isFinished = true, cleaning = false,
			isCleaned = true;

		void Start() {
			slot = new Vector3 (transform.position.x, transform.position.y, -8.0f);
		}

		internal void AddCharacter(Character charac, bool spawned) {
			currentOccupier = charac;
			occupied = true;
			cleaning = false;
			isInRoom = spawned;
		}

		internal void RemoveCharacter() {
			occupied = false;
			isInRoom = false;
			currentOccupier = null;
		}

		void OnMouseDown() {
			GameObject.Find ("GM").GetComponent<GameMechanics> ().selectedRoom = this;
		}

		protected virtual void MoveCharacterToRoom() {
			Vector3 occupierPosition = currentOccupier.transform.position;
			float speed = Time.deltaTime * (Vector3.Distance (occupierPosition, slot) * 2);
			if (speed < Time.deltaTime)
				speed = Time.deltaTime;
			currentOccupier.transform.position = Vector3.MoveTowards (occupierPosition, slot, speed);
			if (Vector3.Distance (currentOccupier.transform.position, slot) <= noMovThreshold)
				noMovFrames--;
			else
				noMovFrames = 3;

			if (noMovFrames == 0) {
				noMovFrames = 3;
				isInRoom = true;
				isFinished = false;
				currentOccupier.isFinished = false;
				Color alpha = this.gameObject.GetComponent<Renderer> ().material.color;
				alpha.g = 0;
				alpha.b = 0;
				this.gameObject.GetComponent<Renderer> ().material.color = alpha;
				startTime = Time.time;
			}
		}

		protected virtual void ProcessRoom() {
			Color alpha = this.gameObject.GetComponent<Renderer> ().material.color;
			alpha.g += Time.deltaTime / procTime;
			alpha.r -= Time.deltaTime / procTime;
			float timer = Time.time - startTime;
			if (timer >= procTime) {
				isFinished = true;
				isCleaned = false;
				currentOccupier.isFinished = true;
				alpha.g = 1f;
				alpha.r = 1f;
				alpha.b = 0f;
			}
			this.gameObject.GetComponent<Renderer> ().material.color = alpha;
		}

		void Update() {
			if (occupied && !isInRoom) {
				MoveCharacterToRoom();
			} else if (occupied && isInRoom && !isFinished && isCleaned) {
				ProcessRoom();
			} else if (!isCleaned && cleaning) {
				Color alpha = this.gameObject.GetComponent<Renderer> ().material.color;
				alpha.b += Time.deltaTime / cleanTime;
				float timer = Time.time - startTime;
				if (timer >= cleanTime) {
					Debug.Log ("CLEAN!");
					isCleaned = true;
					alpha.g = 1f;
					alpha.r = 1f;
					alpha.b = 1f;
					serviceRoom.isBusy = false;
					serviceRoom = null;
				}
				this.gameObject.GetComponent<Renderer> ().material.color = alpha;
			}
		}
	}
}