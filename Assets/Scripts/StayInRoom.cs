﻿using UnityEngine;
using System.Collections;

namespace game {
	public class StayInRoom : Room {
		internal void CleanRoom(ServiceRoom service) {
			serviceRoom = service;
			if (!cleaning) {
				Debug.Log ("Start clean");
				cleaning = true;
				startTime = Time.time;
			}
		}
	}
}
