﻿using UnityEngine;
using System.Collections;

namespace game {
	public class LobbyRoom : Room {
		override protected void MoveCharacterToRoom() {
			Vector3 occupierPosition = currentOccupier.transform.position;
			float speed = Time.deltaTime * (Vector3.Distance (occupierPosition, slot) * 2);
			if (speed < Time.deltaTime)
				speed = Time.deltaTime;
			currentOccupier.transform.position = Vector3.MoveTowards (occupierPosition, slot, speed);
			if (Vector3.Distance (currentOccupier.transform.position, slot) <= noMovThreshold)
				noMovFrames--;
			else
				noMovFrames = 3;
			
			if (noMovFrames == 0) {
				noMovFrames = 3;
				isInRoom = true;
				isFinished = false;
				currentOccupier.isFinished = false;
			}
		}

		override protected void ProcessRoom() {
			Destroy (currentOccupier.gameObject);
			currentOccupier = null;
			isFinished = true;
			occupied = false;
		}
	}
}