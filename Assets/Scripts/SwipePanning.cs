﻿using UnityEngine;
using System.Collections;

public class SwipePanning : MonoBehaviour {
	Vector3 screenPoint, scanPos, offset;

	void OnMouseDown() { 
		scanPos = transform.position;
		screenPoint = Camera.main.WorldToScreenPoint(scanPos);
		offset = scanPos - Camera.main.ScreenToWorldPoint(
			new Vector3(Input.mousePosition.x, screenPoint.y, screenPoint.z));
		
	}
	
	void OnMouseDrag() { 
		Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, screenPoint.y, screenPoint.z);
		Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
		if (curPosition.x < 0 && curPosition.x > -34.7f)
			transform.position = curPosition;
		else if (curPosition.x >= 0)
			transform.position = new Vector3 (0, 0, 0);
		else if (curPosition.x <= -34.7f)
			transform.position = new Vector3 (-34.7f, 0, 0);
	} 
}
