﻿using UnityEngine;
using System.Collections;

namespace game {
	public class Character : MonoBehaviour {
		internal Room currentRoom;
		internal bool isFinished = true;
		void OnMouseDown() {
			if (isFinished)
				GameObject.Find ("GM").GetComponent<GameMechanics> ().selectedCharacter = this;
		}
	}
}
