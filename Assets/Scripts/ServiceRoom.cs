﻿using UnityEngine;
using System.Collections;

namespace game {
	public class ServiceRoom : Room {
		internal bool isBusy = false;
		void OnMouseDown() {
			if (!isBusy) {
				GameObject.Find ("GM").GetComponent<GameMechanics> ().selectedService = this;
			}
		}

		internal void SendCleaner(StayInRoom room) {
			room.CleanRoom(this);
			isBusy = true;
		}
	}
}
