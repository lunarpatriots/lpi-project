﻿using UnityEngine;
using System.Collections;

namespace game {
	public class GameMechanics : MonoBehaviour {
		public Room lobby;
		public GameObject customer;
		public Character selectedCharacter;
		public Room selectedRoom;
		internal ServiceRoom selectedService;

		void Update () {
			if (Time.fixedTime % 7 == 0 && !lobby.occupied)
				SpawnCharacter (lobby);
			else if (selectedRoom != null) {
				if (selectedService != null && selectedRoom.GetComponent<StayInRoom>() != null) {
					selectedService.SendCleaner(selectedRoom.GetComponent<StayInRoom>());
					selectedService = null;
					selectedRoom = null;	
				}
				else if (selectedCharacter == null)
					selectedRoom = null;
				else if (selectedCharacter != null) {
					MoveCharacter (selectedCharacter, selectedRoom);
					selectedRoom = null;
					selectedCharacter = null;
				}
			}
		}

		void SpawnCharacter (Room room) {
			GameObject charac = Instantiate (customer, new Vector3 (room.gameObject.transform.position.x, 
			                                                        room.gameObject.transform.position.y,
			                                                        -8.0f),
			                                 transform.rotation) as GameObject;
			charac.transform.parent = GameObject.Find("Rooms").transform;
			charac.GetComponent<Character> ().currentRoom = room;
			room.AddCharacter (charac.GetComponent<Character>(), true);
		}

		void DespawnCharacter (GameObject charac) {
			Destroy (charac);
		}

		void MoveCharacter (Character charac, Room room) {
			if (room.isFinished && room.isCleaned && !room.occupied) {
				if (charac.currentRoom != null)
					charac.currentRoom.RemoveCharacter ();
				charac.currentRoom = selectedRoom;
				room.AddCharacter (charac, false);
			}
		}
	}
}